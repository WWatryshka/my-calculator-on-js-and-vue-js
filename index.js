let number = document.querySelectorAll('.number');
let table = document.querySelector('.result');
let signs = document.querySelectorAll('.sign');
let plus = document.querySelector('.plus');
let equalButton = document.querySelector('.equal');
let clear = document.querySelector('.clean');
let delOne = document.querySelector('.back');


number.forEach(function(numb){
    numb.addEventListener('click',function (){
        table.value += this.textContent;
    })
})

signs.forEach(function(sign){
    sign.addEventListener('click',function (){
        localSign = this.textContent;
        localEqual = table.value;
        table.value = "";
        
    } )
})

clear.addEventListener('click', function () {
    localSign = {};
    localEqual = {};
})
equalButton.addEventListener('click', equalFunc);

function equalFunc() {
    if(localSign == '+'){
        table.value = parseInt(localEqual) + parseInt(table.value);
    }else if (localSign == '-'){
        table.value = parseInt(localEqual) - parseInt(table.value);
    }else if (localSign == '*') {
        table.value = parseInt(localEqual) * parseInt(table.value);
    }else if (localSign == '/') {
        table.value = parseInt(localEqual) / parseInt(table.value);
    };
    localSign = {};
    localEqual = {};
};

delOne.addEventListener('click', dele)

function dele(){
    var del = table.value;
    table.value = del.substring(0, del.length-1);
}